﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class Page2 : CarouselPage
    {
		public Page2 ()
		{
			InitializeComponent ();
            this.Children.Add(new NewsPage());
            this.Children.Add(new ChatPage());
            this.Children.Add(new SchedulePage());
            
		}
	}
}
