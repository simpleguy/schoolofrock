﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class NewsPage : ContentPage
	{
        NewsView view;
        Models.News news;
        public NewsPage ()
		{
			InitializeComponent ();

            view = new NewsView();
            news = new Models.News {Title = "Новость", Data = "9.07.95", Img = "None", Description = "Простое описание" }; // создание экземляра
            
            stackNews.Children.Add(view);
            view.setNews(news);

            stackNews.Children.Add(
                new NewsView(
                    new Models.News
                    {
                        Title = "Новость дня",
                        Data = "22.02.17",
                        Img = "Типо животрепещющая картинка",
                        Description = "Сегодня вечероим в городе Ульяновск, на улице 12 Сентября ничего не произошло."
                    }
                )
            );

            stackNews.Children.Add(
                new NewsView(
                    new Models.News
                    {
                        Title = "Востание обезьян",
                        Data = "26.02.17",
                        Img = "Картинка обезьяны",
                        Description = "В городском зоопарке обезьяны обявили забостовку и перестали есть. Неужели это начало великого востания"
                    }
                )
            );
        }

    }
}
