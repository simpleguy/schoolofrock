﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class SchedulePage : ContentPage
	{
        Calendar calendar;
        WeekCalendar weekCalendar;
		public SchedulePage ()
		{
			InitializeComponent ();
            calendar = new Calendar(DateTime.Now);
            weekCalendar = new WeekCalendar(DateTime.Now);

            MountButton.Clicked += MountButtonClick;
            WeekButton.Clicked += WeekButtonClick;

            MountButton.IsEnabled = false;
            frameSchedule.Content = calendar;
        }
        void MountButtonClick (object sender, EventArgs e)
        {
            MountButton.IsEnabled = false;
            WeekButton.IsEnabled = true;
            frameSchedule.Content = calendar;
        }
        void WeekButtonClick (object sender, EventArgs e)
        {
            WeekButton.IsEnabled = false;
            MountButton.IsEnabled = true;
            frameSchedule.Content = weekCalendar;
        }
    }
}
