﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class StartPage : ContentPage
	{
		public StartPage ()
		{
			InitializeComponent ();
            registerButton.BackgroundColor = Color.FromRgba(0, 0, 0, 0);
            loginButton.Clicked += getNewsPage;
		}

        void getNewsPage(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.IsEnabled = false;
            
            //await Navigation.PushModalAsync(new Page2());
            App.Current.MainPage = new Page2();
            button.IsEnabled = true;
        }
    }
}
