﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using Xamarin.Forms;
namespace SchoolOfRock.Models
{
    public class News 
    {

        public string Title
        {
            get;
            set;
        }
        public string Data
        {
            get;
            set;
        }
        public string Img
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }
    }
}
