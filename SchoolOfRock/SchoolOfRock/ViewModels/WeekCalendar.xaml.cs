﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class WeekCalendar : ContentView
	{
		public WeekCalendar (DateTime date)
		{
			InitializeComponent ();
            AddWeekCalendar(date);
        }
        void AddWeekCalendar(DateTime date)
        {
            Frame frame;
            //frame.Style = (Style)Application.Current.Resources["defFrame"]; //только так работает
            
            DateTime curTime = date;
            while (curTime.DayOfWeek != DayOfWeek.Monday)
            {
                curTime = curTime.AddDays(-1);
            }
            for (int pos = 0; pos < 7; pos++, curTime =  curTime.AddDays(1))
            {
                frame = new Frame { OutlineColor = Color.Black, Padding = 0 };
                frame.Content = new Label { Text = "lul"};
                weeks.Children.Add(frame, 1, pos);
            }
        }
	}
}
