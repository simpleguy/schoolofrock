﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class Calendar : ContentView
	{
        List<string> mountList = new List<string> { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь" };
		public Calendar(DateTime date)
		{
			InitializeComponent ();
            scheduleMount.Text = mountList[date.Month - 1];
            int week = 1;
            for (DateTime day = new DateTime(date.Year, date.Month, 1); day.Month <= date.Month; day = day.AddDays(1))
            {
                Label label = new Label {
                    Text = day.Day.ToString(),
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center
                };
                if (day.DayOfWeek == DayOfWeek.Sunday || day.DayOfWeek == DayOfWeek.Saturday)
                {
                    label.TextColor = Color.FromHex("#ff3333");
                }
                if (day.CompareTo(DateTime.Today) == 0)
                {
                    label.BackgroundColor = Color.FromHex("#aaccff");
                }
                Schedule.Children.Add(
                    label,
                    NumberDayOfWeek(day.DayOfWeek),
                    week
                    );

                if (day.DayOfWeek == DayOfWeek.Sunday)
                {
                    week++;
                }
            }
		}

        int NumberDayOfWeek(DayOfWeek week)
        {
            switch(week)
            {
                case DayOfWeek.Monday:
                    return 0;
                case DayOfWeek.Tuesday:
                    return 1;
                case DayOfWeek.Wednesday:
                    return 2;
                case DayOfWeek.Thursday:
                    return 3;
                case DayOfWeek.Friday:
                    return 4;
                case DayOfWeek.Saturday:
                    return 5;
                case DayOfWeek.Sunday:
                    return 6;
            }
            return 0;
        }
	}
}
