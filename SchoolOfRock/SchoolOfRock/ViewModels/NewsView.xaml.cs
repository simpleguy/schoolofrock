﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SchoolOfRock
{
	public partial class NewsView : ContentView
	{  
		public NewsView ()
		{
			InitializeComponent ();
		}
        public NewsView(Models.News news)
        {
            InitializeComponent();
            setNews(news);
        }
        public void setNews(Models.News news)
        {
            /*
            Title.SetBinding(Label.TextProperty, new Binding { Source = "news", Path = "Title" });
            Data.SetBinding(Label.TextProperty, new Binding { Source = "news", Path = "Data" });
            Description.SetBinding(Label.TextProperty, new Binding { Source = "news", Path = "Description" });
            */
            Title.Text = news.Title;
            Data.Text = news.Data;
            Img.Text = news.Img;
            Description.Text = news.Description;
        }
	}
}
